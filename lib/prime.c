#include <stdio.h>
#include <stdbool.h>

bool isPrime(int num) {
  for (int i = 2; i < num; i++) {
    if (num % i == 0) {
      return false;
    }
  }

  return (num != 1 && num != 0) ? true : false;
}

int checkPrimes(int num) {
  int found = 0;
  for (int i = 0; i < num; i++) {
    if (isPrime(i)) {
      found++;
    }
  }
  return found;
}