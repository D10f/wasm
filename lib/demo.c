#include <stdio.h>
#include <string.h>

char *greet(char *name);

int main() {

  printf("Wasm ready\n");

  return 1;
}

int getNum() {
  return 42;
}

int getDoubleNum(int x) {
  return x * 2;
}

char greeting[50];
char *greet(char *name) {
  if (strlen(name) > 40) {
    return "Name too long";
  }

  strcpy(greeting, "Hello, ");
  strcat(greeting, name);
  return greeting;
}