#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <emscripten.h>

#define TOTAL_CIRCLES 500

typedef struct circle_t {
  int x;        ///< Circle x position
  int y;        ///< Circle y position
  int r;        ///< Circle radius
  int cr;       ///< Red color component
  int cg;       ///< Green color component
  int cb;       ///< Blue color component
} circle_t;

typedef struct circle_animation_t {
  int x;
  int y;
  int r;
  int vx;    ///< Velocity on the x axis
  int vy;    ///< Velocity on the y axis
  int dx;    ///< Direction on the x axis (-1 left, 1 right)
  int dy;    ///< Direction on the y axis (-1 top,  1 bot)
} circle_animation_t;

circle_t circles[TOTAL_CIRCLES];
circle_animation_t animationData[TOTAL_CIRCLES];

void updateCircles(int width, int height) {
  for (int i = 0; i < TOTAL_CIRCLES; i++) {

    // Detect collision on x axis
    if (animationData[i].x + animationData[i].r >= width) {
      animationData[i].dx = -1;
    } else if (animationData[i].x - animationData[i].r <= 0) {
      animationData[i].dx = 1;
    }

    // Detect collision on y axis
    if (animationData[i].y + animationData[i].r >= height) {
      animationData[i].dy = -1;
    } else if (animationData[i].y - animationData[i].r <= 0) {
      animationData[i].dy = 1;
    }

    // Increase position based on speed
    animationData[i].x += animationData[i].vx * animationData[i].dx;
    
    // if (animationData[i].dy == 1) {
    animationData[i].y += animationData[i].vy * animationData[i].dy;

    circles[i].x = animationData[i].x;
    circles[i].y = animationData[i].y;
  }
}

circle_t *getCircles() {
  return circles;
}

int getRand(int min, int max) {
  return (rand() % max) + min;
}

int main() {
  printf("Init circles\n");

  // seed random number generator
  srand(time(NULL));

  // create circle data
  for (int i = 0; i < TOTAL_CIRCLES; i++) {
    int r = getRand(20, 50);
    int x = getRand(r, 1000);
    int y = getRand(r, 1000);

    circles[i].x = x;
    circles[i].y = y;
    circles[i].r = r;
    circles[i].cr = getRand(0, 255);
    circles[i].cg = getRand(0, 255);
    circles[i].cb = getRand(0, 255);

    animationData[i].x = x;
    animationData[i].y = y;
    animationData[i].r = r;
    animationData[i].vx = getRand(2, 18);
    animationData[i].vy = getRand(2, 18);
    animationData[i].dx = 1;
    animationData[i].dy = 1;
  }

  // invoke JavaScript's render function
  // emscripten_run_script("render()");

  // invoke JavaScript't render function with arguments
  EM_ASM({ render($0); }, 6 * TOTAL_CIRCLES);
}