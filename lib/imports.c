#include <stdio.h>
#include <emscripten.h>

// declare reusable js function
EM_JS(void, jsFunction, (int n), {
  console.log('Call from EM_JS');
});

int main() {

  printf("Wasm ready\n");

  // Call JS function using eval
  emscripten_run_script("console.log('hello from c!!')");

  // Call JS function after some time in miliseconds
  emscripten_async_run_script("console.log('Hello after 2500ms')", 2500);

  // Get return value from JS function - integer
  int jsInt = emscripten_run_script_int("Math.floor(Math.random() * 25) + 1");
  printf("%d\n", jsInt);

  // Get return value from JS function - string
  char *jsString = emscripten_run_script_string("'fdsfsdfs'.toUpperCase()");
  printf("%s\n", jsString);

  jsFunction(2);

  return 1;
}