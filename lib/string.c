#include <stdio.h>
#include <emscripten.h>

char *getStr() {
  return "Hello World";
}

int getNum() {
  return 22;
}

int main() {
  printf("Wasm ready\n");
  return 0;
}